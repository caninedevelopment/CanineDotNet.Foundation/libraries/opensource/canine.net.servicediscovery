﻿namespace Canine.Net.ServiceDiscovery.Implementation
{
    using Common.Interfaces;
    using System;
    public class MachineTime : IDateTime
    {
        public DateTime Now => DateTime.Now;
    }
}
