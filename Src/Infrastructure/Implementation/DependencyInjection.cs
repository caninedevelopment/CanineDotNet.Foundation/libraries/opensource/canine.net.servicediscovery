namespace Canine.Net.ServiceDiscovery.Implementation
{
    using Canine.Net.ServiceDiscovery.Application.Common.Interfaces;
    using Canine.Net.ServiceDiscovery.Common.Interfaces;
    using Microsoft.Extensions.DependencyInjection;
    public static class DependencyInjection
    {
        public static IServiceCollection AddImplementation(this IServiceCollection services)
        {
            services.AddTransient<IDateTime, MachineTime>();
            services.AddTransient<ICanineService, CanineService>();
            return services;
        }
    }
}