﻿namespace Canine.Net.ServiceDiscovery.Implementation
{
    using Application.Common.Interfaces;
    using Canine.Net.Infrastructure.RabbitMQ.Message;
    using Canine.Net.Infrastructure.RabbitMQ.Request;
    using Common.Interfaces;

    public class CanineService : ICanineService
    {
        private readonly IDateTime datetime;

        public CanineService(IDateTime datetime)
        {
            this.datetime = datetime;
        }
        public void ExecuteServiceDiscoveryHelp()
        {
            RequestClient.FAFInstance.FAF("servicediscovery.help",
                new RabbitMqHeader()
                {
                    ClientId = "N/A",
                    Ip = "N/A",
                    IsDirectLink = false,
                    MessageId = "N/A",
                    MessageIssueDate = datetime.Now,
                    TokenInfo = null, /*TODO!*/
                },
                "{}",
                ""
            );
        }
    }
    }
