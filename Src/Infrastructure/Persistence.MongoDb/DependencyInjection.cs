namespace Canine.Net.ServiceDiscovery.Infrastructure.Persistence.MongoDb
{
    using Microsoft.Extensions.DependencyInjection;
    public static class DependencyInjection
    {
        public static IServiceCollection AddMongoDbPersistenceProvider(this IServiceCollection services)
        {
            return services;
        }
    }
}