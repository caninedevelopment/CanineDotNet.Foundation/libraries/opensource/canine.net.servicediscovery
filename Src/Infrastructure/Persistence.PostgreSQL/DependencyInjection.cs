namespace Canine.Net.ServiceDiscovery.Infrastructure.Persistence.PostgreSQL
{
    using Canine.Net.ServiceDiscovery.Persistence.PostgreSQL;
    using Canine.Net.ServiceDiscovery.Persistence.PostgreSQL.Interfaces;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.DependencyInjection;
    using Canine.Net.ServiceDiscovery.Persistence.PostgreSQL.Repositories;

    public static class DependencyInjection
    {
       // private const string connectionVariableName = "PostgreConnectionString";
        public static IServiceCollection AddPostgreSQLPersistenceProvider(this IServiceCollection services, string dbServerHost, string dbUser, string dbPassword, int dbServerPort, string dbName)
        {
            services.AddDbContext<ServiceDbContext>(options => options.UseNpgsql($"Host={dbServerHost};User ID={dbUser};Password={dbPassword};Port={dbServerPort};Database={dbName};"));
            services.AddScoped<IServiceDbContext>(provider => provider.GetService<ServiceDbContext>());

            services.AddScoped<Domain.Repositories.ServiceRepository,ServiceRepository>();
            return services;
        }
    }
}