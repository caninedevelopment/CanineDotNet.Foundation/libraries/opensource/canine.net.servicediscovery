﻿using Canine.Net.Infrastructure.RabbitMQ.DTO;
using Canine.Net.ServiceDiscovery.Domain.Interfaces;
using System;

namespace Canine.Net.ServiceDiscovery.Persistence.PostgreSQL.Entities
{
    public class Service : IAuditable
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Descriptor { get; set; }
        public DateTime LastSeen { get; set; }
        public DateTime? LastModified { get; set; }
        public DateTime Created { get; set; }

        public Service()
        {
        }
        public Service(Domain.Entities.Service service)
        {
            Id = service.Id;
            Name = service.Name;
            Descriptor = Newtonsoft.Json.JsonConvert.SerializeObject(service.Descriptor);
            LastSeen = service.LastSeen;
        }
        public Domain.Entities.Service AsDomain()
        {
            return new Domain.Entities.Service
            {
                Id = this.Id,
                Name = this.Name,
                Descriptor = Newtonsoft.Json.JsonConvert.DeserializeObject<ServiceDescriptor>(this.Descriptor),
                LastSeen = this.LastSeen,
            };
        }
    }
}
