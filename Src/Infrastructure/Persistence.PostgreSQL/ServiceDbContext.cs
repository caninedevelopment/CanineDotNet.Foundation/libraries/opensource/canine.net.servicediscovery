﻿namespace Canine.Net.ServiceDiscovery.Persistence.PostgreSQL
{
    using Canine.Net.ServiceDiscovery.Persistence.PostgreSQL.Entities;
    using Canine.Net.ServiceDiscovery.Persistence.PostgreSQL.Interfaces;
    using Common.Interfaces;
    using Domain.Interfaces;
    using Microsoft.EntityFrameworkCore;
    using Npgsql;

    public class ServiceDbContext : DbContext, IServiceDbContext
    {
        private readonly IDateTime dateTime;

        public ServiceDbContext(DbContextOptions<ServiceDbContext> options, IDateTime dateTime)
            : base(options)
        {
            this.dateTime = dateTime;
            this.Database.EnsureCreated();
        }

        public DbSet<Service> Services { get; set; }

        public override int SaveChanges()
        {
            foreach (Microsoft.EntityFrameworkCore.ChangeTracking.EntityEntry<IAuditable> entry in ChangeTracker.Entries<IAuditable>())
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.Entity.Created = dateTime.Now;
                        entry.Entity.LastModified = dateTime.Now;
                        break;

                    case EntityState.Modified:
                        entry.Entity.LastModified = dateTime.Now;
                        break;
                }
            }

            return base.SaveChanges();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(ServiceDbContext).Assembly);
        }
    }
}
