﻿namespace Canine.Net.ServiceDiscovery.Persistence.PostgreSQL.Configuration
{
    using Canine.Net.ServiceDiscovery.Persistence.PostgreSQL.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    public class ServiceConfiguration : IEntityTypeConfiguration<Service>
    {
        public void Configure(EntityTypeBuilder<Service> builder)
        {
        }
    }
}
