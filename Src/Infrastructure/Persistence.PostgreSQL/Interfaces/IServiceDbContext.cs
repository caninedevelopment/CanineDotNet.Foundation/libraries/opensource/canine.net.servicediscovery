﻿namespace Canine.Net.ServiceDiscovery.Persistence.PostgreSQL.Interfaces
{
    using Canine.Net.ServiceDiscovery.Persistence.PostgreSQL.Entities;
    using Microsoft.EntityFrameworkCore;

    public interface IServiceDbContext
    {
        DbSet<Service> Services { get; set; }
        int SaveChanges();
    }
}
