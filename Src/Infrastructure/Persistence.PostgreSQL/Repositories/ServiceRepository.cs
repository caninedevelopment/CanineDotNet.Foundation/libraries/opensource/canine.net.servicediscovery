﻿namespace Canine.Net.ServiceDiscovery.Persistence.PostgreSQL.Repositories
{
    using Common.Interfaces;
    using Domain.Entities;
    using Persistence.PostgreSQL.Interfaces;
    using System.Collections.Generic;
    using System.Linq;

    public class ServiceRepository : Domain.Repositories.ServiceRepository
    {
        private readonly IServiceDbContext serviceDbContext;

        public ServiceRepository(IServiceDbContext serviceDbContext, IDateTime dateTime) : base(dateTime)
        {
            this.serviceDbContext = serviceDbContext;
        }

        public override void Clean()
        {
            serviceDbContext.Services.RemoveRange(serviceDbContext.Services);
            serviceDbContext.SaveChanges();
        }

        public override List<Service> GetAll()
        {
            return serviceDbContext.Services.ToList().Select(t => t.AsDomain()).ToList();
        }

        public override void InsertMany(List<Service> entities)
        {
            serviceDbContext.Services.AddRange(entities.Select(service => new Entities.Service(service)));
            serviceDbContext.SaveChanges();
        }

        public override void InsertOrUpdateMany(List<Service> entities)
        {
            foreach (var service in entities)
            {
                service.LastSeen = _datetime.Now;
            }
            var serviceNames = entities.Select(service => service.Name).ToList();
            var existingServices = serviceDbContext.Services.Where(service => serviceNames.Contains(service.Name)).ToList();
            foreach (var existingService in existingServices)
            {
                var updatedService = entities.First(service => service.Name == existingService.Name);
                existingService.Descriptor = Newtonsoft.Json.JsonConvert.SerializeObject(updatedService.Descriptor);
                existingService.LastSeen = updatedService.LastSeen;
            }
            serviceDbContext.Services.UpdateRange(existingServices);

            var existingServiceNames = existingServices.Select(service => service.Name).ToList();
            var newServices = entities.Where(service => !existingServiceNames.Contains(service.Name)).ToList();
            serviceDbContext.Services.AddRange(newServices.Select(service => new Entities.Service(service)));
            serviceDbContext.SaveChanges();
        }
    }
}
