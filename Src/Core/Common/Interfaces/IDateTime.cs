﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Canine.Net.ServiceDiscovery.Common.Interfaces
{
    public interface IDateTime
    {
        DateTime Now { get; }
    }
}
