namespace Canine.Net.ServiceDiscovery.Application.Resources.Services.Commands.GetAll
{
    using Common.Extensions;
    using Domain.Entities;
    using Domain.Repositories;
    using Monosoft.Common.Command.Interfaces;
    using System.Collections.Generic;

    public class Command : IFunction<Response>
    {
        private readonly ServiceRepository repository;

        public Command(ServiceRepository repository)
        {
            this.repository = repository;
        }

        public Response Execute()
        {
            return repository.GetAll().Map<List<Service>, Response>();
        }
    }
}