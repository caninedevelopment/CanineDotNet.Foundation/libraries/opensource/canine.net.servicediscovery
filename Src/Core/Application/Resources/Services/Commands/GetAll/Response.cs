namespace Canine.Net.ServiceDiscovery.Application.Resources.Services.Commands.GetAll
{
    using Application.Common.Interfaces;
    using Canine.Net.Infrastructure.RabbitMQ.DTO;
    using Domain.Entities;
    using Monosoft.Common.Command.Interfaces;
    using System.Collections.Generic;
    using System.Linq;

    public class Response : ServiceDescriptors, IDtoResponse, IMapFrom<List<Service>>
    {
        public void MapFrom(List<Service> source)
        {
            this.Services = source.Select(s => s.Descriptor).ToList();
        }
    }
}