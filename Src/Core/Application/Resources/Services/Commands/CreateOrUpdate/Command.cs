﻿namespace Canine.Net.ServiceDiscovery.Application.Resources.Services.Commands.CreateOrUpdate
{
    using Canine.Net.Infrastructure.RabbitMQ.DTO;
    using Domain.Entities;
    using Domain.Repositories;
    using Monosoft.Common.Command.Interfaces;
    using Newtonsoft.Json;
    using System.Linq;
    using System.Text;

    public class Command : IProcedure<Request>
    {
        private readonly ServiceRepository _serviceRepository;

        public Command(ServiceRepository serviceRepository)
        {
            _serviceRepository = serviceRepository;
        }
        public void Execute(Request input)
        {
            var json = Encoding.UTF8.GetString(input.Message.Data);
            var msg = JsonConvert.DeserializeObject<ServiceDescriptors>(json);
            var services = msg.Services.Where(service => !string.IsNullOrEmpty(service.Name)).Select(service => new Service { Name = service.Name, Descriptor = service }).ToList();
            _serviceRepository.InsertOrUpdateMany(services);
        }
    }
}
