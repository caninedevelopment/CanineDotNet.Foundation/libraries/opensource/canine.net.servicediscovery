﻿namespace Canine.Net.ServiceDiscovery.Application.Resources.Services.Commands.CreateOrUpdate
{
    using Canine.Net.Infrastructure.RabbitMQ.Message;
    using Monosoft.Common.Command.Interfaces;

    public class Request : IDtoRequest
    {
        public ReturnMessage Message { get; set; }
        public void Validate()
        {
            if(Message is null)
                throw new System.ArgumentNullException(nameof(Message));
        }
    }
}