namespace Canine.Net.ServiceDiscovery.Application.Resources.Services.Commands.Scan
{
    using Application.Common.Interfaces;
    using Domain.Repositories;
    using Monosoft.Common.Command.Interfaces;

    public class Command : IProcedure
    {
        private readonly ServiceRepository repository;
        private readonly ICanineService canineService;

        public Command(ServiceRepository repository, ICanineService canineService)
        {
            this.repository = repository;
            this.canineService = canineService;
        }

        public void Execute()
        {
            repository.Clean();
            canineService.ExecuteServiceDiscoveryHelp();
        }
    }
}