﻿namespace Canine.Net.ServiceDiscovery.Application.Common.Extensions
{
    using Common.Interfaces;
    using System;

    public static class Mapper
    {
        public static TResult Map<TSource, TResult>(this TSource source) where TSource : new() where
            TResult : IMapFrom<TSource>
        {
            TResult result = (TResult)Activator.CreateInstance(typeof(TResult));
            result.MapFrom(source);
            return result;
        }
    }
}