﻿namespace Canine.Net.ServiceDiscovery.Application.Common.Interfaces
{
    public interface ICanineService
    {
        void ExecuteServiceDiscoveryHelp();
    }
}
