﻿namespace Canine.Net.ServiceDiscovery.Application.Common.Interfaces
{
    public interface IMapFrom<in TSource> where TSource : new()
    {
        void MapFrom(TSource source);
    }
}
