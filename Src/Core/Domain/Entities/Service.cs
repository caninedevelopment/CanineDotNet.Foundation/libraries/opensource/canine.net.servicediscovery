﻿namespace Canine.Net.ServiceDiscovery.Domain.Entities
{
    using Canine.Net.Infrastructure.RabbitMQ.DTO;
    using System;

    public class Service
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ServiceDescriptor Descriptor { get; set; }
        public DateTime LastSeen { get; set; }
    }
}
