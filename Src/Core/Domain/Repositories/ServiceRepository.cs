﻿namespace Canine.Net.ServiceDiscovery.Domain.Repositories
{
    using Common.Interfaces;
    using Entities;
    using Interfaces;
    using System.Collections.Generic;

    public abstract class ServiceRepository : IRepository<Service>
    {
        protected readonly IDateTime _datetime;

        public ServiceRepository(IDateTime datetime)
        {
            _datetime = datetime;
        }

        public virtual void Clean()
        {
            throw new System.NotImplementedException();
        }

        public virtual List<Service> GetAll()
        {
            throw new System.NotImplementedException();
        }

        public virtual void InsertMany(List<Service> entities)
        {
            throw new System.NotImplementedException();
        }

        public virtual void InsertOrUpdateMany(List<Service> entities)
        {
            throw new System.NotImplementedException();
        }
    }
}
