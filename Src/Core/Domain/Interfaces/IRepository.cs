﻿using System.Collections.Generic;

namespace Canine.Net.ServiceDiscovery.Domain.Interfaces
{
    public interface IRepository<T>
    {
        List<T> GetAll();
        void Clean();
        void InsertMany(List<T> entities);
        void InsertOrUpdateMany(List<T> entities);
    }
}
