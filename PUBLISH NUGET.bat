set /p ver="Version: "

dotnet build .\Canine.Net.ServiceDiscovery.sln -o Artifacts/%ver% -p:Version=%ver%
nuget push .\Artifacts\%ver%\*%ver%.nupkg -Source Monosoft -NonInteractive